# README #

This is an example of C++ project build with "Modern CMake" ideas.

Also, this project uses [docopt](https://github.com/docopt/docopt.cpp) for command line options parsing, so it's a kind of example for _docopt_ usage.

### More Info ###

More information about _modern cmake_:

* [It's Time To Do CMake Right](https://pabloariasal.github.io/2018/02/19/its-time-to-do-cmake-right/)
* [How to Use Modern CMake for an App + Lib Project](https://code.egym.de/how-to-use-modern-cmake-for-an-app-lib-project-3c2ee6018cde)
* [Mathieu Ropert] at CppCon 2017, "Using Modern CMake Patterns to Enforce a Good Modular Design"
* [Daniel Pfeifer](https://www.youtube.com/watch?v=bsXLMQ6WgIk) C++Now 2017, “Effective CMake"

### Details ###

The application does following:
* basic functionality is similar to "echo". It prints back a string received from command line
* also user can add "-r/--repeat N" option and the string will be printed N times
* also user can add "-b/--box" option and the string will be printed wrapped with "|" characters

Sources consist of two parts:
* the application itself
* command line options processor. This part was taken from [docopt](https://github.com/docopt/docopt.cpp) project.

