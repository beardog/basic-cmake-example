#pragma once

#include <string>

struct AppSettings {
  bool printBoxed;
  int repeat;
  std::string line;
};

enum ClopErrorCode {
  ERR_NoError = 0,
  ERR_BadBoxedValue,
  ERR_BadRepeatValue,
  ERR_BadLineValue
};

ClopErrorCode processCommandLineOptions(int argc, char *argv[], AppSettings& appSettings);

