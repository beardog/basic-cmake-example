#include "ProcessCLO.h"

#include <iostream>
using namespace std;

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

int main(int argc, char *argv[]) {

  AppSettings appSettings = {
    .printBoxed = false,
    .repeat = 1,
    .line = ""
  };

  const ClopErrorCode clopErrorCode = processCommandLineOptions(argc, argv, appSettings);
  if (clopErrorCode!= ERR_NoError) {
    cout << "Some logical error appeared"
         << "(" << clopErrorCode << ")" << endl;
    return 1;
  }

  if (appSettings.repeat == 0) {
    return 0;
  }

  if (appSettings.printBoxed) {
    for(int i = 0; i< appSettings.repeat; i++) {
      cout << "| " << appSettings.line << " |" << endl;
    }
  }
  else {
    for(int i = 0; i< appSettings.repeat; i++) {
      cout << appSettings.line << endl;
    }
  }

  return 0;
}

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
