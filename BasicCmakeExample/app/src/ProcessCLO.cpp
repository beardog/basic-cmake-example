#include "ProcessCLO.h"

#include "docopt.h"

#include <iostream> //cout
using namespace std;

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

static const char USAGE[] =
  R"(necho works like echo but can repeat line few times

    Usage:
      necho [-b | --boxed] [-r <count> | --repeat=<count>] [<string_to_print>]...
      necho (-h | --help)
      necho --version

    Options:
      -h, --help            Show this screen.
      --version             Show version.
      -r=<N>, --repeat=<N>  Defines amount of repeats.
      -b, --boxed           Prints line with some prefix and suffix.
)";

// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

ClopErrorCode processCommandLineOptions(int argc, char *argv[], AppSettings& appSettings) {
  map<string, docopt::value> args = docopt::docopt(USAGE,
                                                   { argv + 1, argv + argc },
                                                   true,    // show help if requested
                                                   "necho 0.0.1"); // version string

  // cout << "Arguments are:" << endl;
  // for(auto const& arg : args) {
  //   cout << arg.first << ": " << arg.second << std::endl;
  // }
  auto searchBoxed = args.find("--boxed");
  if (searchBoxed!=args.end()) {
    docopt::value v = searchBoxed->second;
    if (v.isBool()) {
      appSettings.printBoxed = v.asBool();
    }
    else {
      return ERR_BadBoxedValue;
    }
  }

  const auto searchRepeat = args.find("--repeat");
  if (searchRepeat!=args.end()) {
    const docopt::value v = searchRepeat->second;
    if (v.isLong()) {
      appSettings.repeat = v.asLong();
    }
    else {
      // cout << "Err: repeat type is" << (int)v.kind() << endl;
      // try to convert int to string
      if (v.isString()) {
        appSettings.repeat = stoi(v.asString());
      }
      else {
        return ERR_BadRepeatValue;
      }
    }
  }

  const auto searchLine = args.find("<string_to_print>");
  if (searchLine!=args.end()) {
    const docopt::value v = searchLine->second;
    if (v.isString()) {
      appSettings.line = v.asString();
    }
    else {
      if (v.isStringList()) {
        const vector<string> vv = v.asStringList();
        if (vv.empty()) {
          appSettings.line = "";
        }
        else {
          auto vi = vv.cbegin();
          appSettings.line = *vi;
          vi++;
          for (vi; vi != vv.cend(); vi++ ) {
            appSettings.line =  appSettings.line + " " + *vi;
          }
        }
      }
      else {
        return ERR_BadRepeatValue;
      }

    }
  }

  // finally
  return ERR_NoError;
}